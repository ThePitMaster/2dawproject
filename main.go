package main

import "ToLearnMyProject/infrastructure"

func main() {
	router := infrastructure.NewGinRouter()
	router.Gin.Run(":8000") //running application, Default port is 8080
}
