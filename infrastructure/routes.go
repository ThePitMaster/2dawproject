package infrastructure

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type GinRouter struct {
	Gin *gin.Engine
}

func NewGinRouter() GinRouter {
	router := gin.Default() //new gin router initialization
	router.GET("/", func(context *gin.Context) {
		LoadEnv()     //loading env
		NewDatabase() //new database connection
		context.JSON(http.StatusOK, gin.H{"data": "I am Working!"})
	}) // first endpoint returns Hello World
	return GinRouter{
		Gin: router,
	}
}
