#What is a Dockerfile?
#A Dockerfile is a text document that CONTAIS all the
#COMMANDS a user could call on the command line TO ASEMBLE AN IMAGE.
FROM golang:alpine
#REFERS the BASE IMAGE the CONTAINER will be BASED OF

RUN mkdir /app
#RUN EXECUTES COMMANDS INSIDE the CONTAINER


WORKDIR /app
#WORKDIR SETS THE WORKING DIRECTORY FOR ANY RUN, CMD, ENMTRYPOINT, COPY and ADD

ADD go.mod .
ADD go.sum .
#ADD COPIES NEW FILES, DIRECTORIES or REMOTE FILE from <src> and adds them to the image

RUN go mod download
ADD . .

RUN go install github.com/githubnemo/CompileDaemon@latest
RUN apk add --no-cache git
#Watches your .go files in a directory and invokes go build if a file changed. Nothing more.
#https://github.com/githubnemo/CompileDaemon/issues/77
#"the reason is:
  #golang:1.17.8-alpine came with the git executable
  #- presumably to accommodate go get - but golang:1.18-alpine does not
  #- presumably because of the breaking change to go get that accompanied the new version."

EXPOSE 8000
#EXPOSE INFORMS Docker that the CONTAINER LISTENS ON SPECIFIED NETWORK PORTS at RUNTIME

ENTRYPOINT CompileDaemon --build="go build main.go" --command="./main"
#ENTRYPOINT RUNS the COMMAND INSIDE the CONTAINER ONCE it is created from an image
